<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="pool-name" />
  <xsl:param name="pool-path" />
  <xsl:param name="pool-mode" />
  <xsl:param name="pool-owner" />
  <xsl:param name="pool-group" />

  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="name">
    <name><xsl:value-of select="$pool-name" /></name>
  </xsl:template>

  <xsl:template match="path">
    <path><xsl:value-of select="$pool-path" /></path>
  </xsl:template>

  <xsl:template match="mode">
    <mode><xsl:value-of select="$pool-mode" /></mode>
  </xsl:template>

  <xsl:template match="owner">
    <owner><xsl:value-of select="$pool-owner" /></owner>
  </xsl:template>

  <xsl:template match="group">
    <group><xsl:value-of select="$pool-group" /></group>
  </xsl:template>

</xsl:stylesheet>
