# Simple VMs with libvirt

## pool

A `pool` is where the `volumes` will be stored.

```ShellSession
virsh pool-create-as --name simple-pool --type dir --target $PWD/pool --build
```

To create it with proper ownership it is necessary to create the pool by XML.
It's possible to process the template with the following command:

```ShellSession
xsltproc \
  --stringparam pool-name "test" \
  --stringparam pool-path "${PWD}/pool" \
  --stringparam pool-mode 0775 \
  --stringparam pool-owner "$UID" \
  --stringparam pool-group "$GID" \
  templates/pool.xsl templates/pool.xml
```

## volume

A `volume` is a reference to a disk image. Images can be a ISO, a RAW, a QCOW2
file...

To create the image volume:

```ShellSession
virsh vol-create-as default debian.qcow2 5G --format qcow2
```

To create the ISO volume:

```ShellSession
virsh vol-create-as default debian10.iso 301M --format raw
virsh vol-upload debian10.iso ../debian-10-amd64-CD-1.iso default
```

## References

* [Libvirt volume
    management](https://www.cipheronic.com/libvirt-volume-management/)
* [USB controller for q35
    machines](https://bugzilla.redhat.com/show_bug.cgi?id=1527860)_

