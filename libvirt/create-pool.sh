#!/usr/bin/env bash

POOL_NAME="simple-pool"
POOL_PATH="$PWD/pool"
POOL_MODE=0775
POOL_OWNER="$(id -u)"
POOL_GROUP="$(id -g)"

xsltproc \
  --stringparam pool-name "$POOL_NAME" \
  --stringparam pool-path "$POOL_PATH" \
  --stringparam pool-mode "$POOL_MODE" \
  --stringparam pool-owner "$POOL_OWNER" \
  --stringparam pool-group "$POOL_GROUP" \
  templates/pool.xsl templates/pool.xml > pool.xml
virsh pool-define pool.xml
virsh pool-start "$POOL_NAME"
virsh pool-autostart "$POOL_NAME"
rm pool.xml
