# Simple VMs with QEMU

Run simple VMs using QEMU and VirtIO.

## Create image

```ShellSession
qemu-img create -f qcow2 debian.qcow2 20G
```
## References

* https://www.arthurkoziel.com/qemu-ubuntu-20-04/
* https://www.cyberciti.biz/faq/howto-setup-serial-console-on-debian-linux/
* https://teklager.se/en/knowledge-base/installing-debian-over-serial-console-apu-board/
* https://serverfault.com/questions/769874/how-to-forward-a-port-from-guest-to-host-in-qemu-kvm
* https://wiki.qemu.org/Features/VirtioSCSI
* https://wiki.qemu.org/Documentation/Networking
* https://blogs.oracle.com/linux/how-to-emulate-block-devices-with-qemu
* https://mpolednik.github.io/2017/01/23/virtio-blk-vs-virtio-scsi/
* https://www.reddit.com/r/VFIO/comments/79wfgd/add_virtio_based_cdrom_in_qemu/
* https://wiki.gentoo.org/wiki/QEMU/Options
* https://github.com/WMCB-Tech/qemu-run-scripts

