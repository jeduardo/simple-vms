#!/usr/bin/env bash

function help_and_exit() {
    echo "$0 <disk.qcow2> <cdrom.iso>"
    exit 1
}

OS=$(uname -s)
case $OS in
    "Linux")
        ACCEL=kvm
        ;;
    "Darwin")
        ACCEL=hvf
        ;;
esac

DISK=$1
CDROM=$2

if [ -z "$DISK" ]; then
    help_and_exit
fi

if [ -z "$CDROM" ]; then
    help_and_exit
fi

qemu-system-x86_64 \
    -machine type=q35,accel=$ACCEL \
    -smp 4 \
    -m 2G \
    -serial mon:stdio \
    -device usb-tablet \
    -usb \
    -net user,hostfwd=tcp::8022-:22 \
    -nic user,model=virtio-net-pci \
    -boot order=cd \
    -device virtio-scsi-pci,id=scsi0,num_queues=4 \
    -device scsi-hd,drive=drive0,bus=scsi0.0,channel=0,scsi-id=0,lun=0 \
    -device scsi-cd,drive=cdrom0,bus=scsi0.0,channel=0,scsi-id=0,lun=1 \
    -drive file="${DISK}",if=none,id=drive0,format=qcow2 \
    -drive file="${CDROM}",if=none,id=cdrom0,format=raw \
    -device virtio-balloon \
    -device virtio-serial-pci,id=virtio-serial0 \
    -device virtio-gpu-pci \
    -device virtio-tablet-pci \
    -device virtio-mouse-pci \
    -device virtio-keyboard-pci \
    -nographic \
    -vga none \
#    -device virtio-rng-pci,rng=rng0,id=virtio-rng-pci0 \
#    -object rng-random,filename=/dev/urandom,id=rng0 \
#    -boot menu=on \
#    -chardev tty,id=charconsole0,mux=on,path=$(tty) \
#    -device virtconsole,chardev=charconsole0,id=console0 \
#    -hda debian.qcow2 \
#    -cdrom debian.iso \
