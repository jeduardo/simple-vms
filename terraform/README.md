# Simple VMs with Terraform

Using the [libvirt provider for terraform](https://github.com/dmacvicar/terraform-provider-libvirt/),
it is possible to use terraform to provision, through [libvirt](https://libvirt.org/),
KVM-based virtual machines with VirtIO devices.

## libvirtd configuration

By default on Debian-based systems, libvirtd will create images with a
permission set for root:root, making it impossible for users belonging to the
libvirtd-qemu group to be able to manipulate images and, worse, use the
terraform provider to load the images, volumes, and VMs. 

To avoid it, one must set the `security_driver` for qemu to `"none"` in the config
file `/etc/libvirt/qemu.conf`, as mentioned in the comments for the 
[issue](https://github.com/dmacvicar/terraform-provider-libvirt/issues/546)
where this problem is discussed.

## Terraform plugin installation

The provider needs to be installed manually into the user workspace that will be
running Terraform as per the [provider installation
instructions](https://github.com/dmacvicar/terraform-provider-libvirt/blob/master/docs/migration-13.md).


