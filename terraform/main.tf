terraform {
 required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.3"
    }
  }

   backend "local" {
    path = "debian.tfstate"
  } 
}

provider "libvirt" {
  uri = "qemu:///system"
}

resource "libvirt_network" "test-net" {
  name = "test-net"
  mode = "nat"
  domain = "net.test"
  addresses = ["12.0.0.0/24"]
  autostart = "true"
}

# Master image
resource "libvirt_volume" "debian" {
  name = "debian-source.qcow2"
  source = "file:///home/jeduardo/projects/simple-vms/debian.qcow2"
}

# Instance image
resource "libvirt_volume" "instance" {
  name = "instance.qcow2"
  base_volume_id = libvirt_volume.debian.id
}

# Instance
resource "libvirt_domain" "instance" {
  name = "instance"
  vcpu = 2
  memory = 1024

  disk {
    volume_id = libvirt_volume.instance.id
  }

  boot_device {
    dev = ["cdrom", "hd"]
  }

  # tty0 for virsh console
  console {
    type = "pty"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }

  network_interface {
    # requires qemu-agent
    wait_for_lease = false
  }
}

